openApp(){
    local desc=$1
    shift
    while [[ $# -gt 0 ]] do
        desc+=' '
        desc+=${1}
        shift
    done
  open -n -a $desc
}
# run.docker(){ # Doesn't work
#   open -n -a ~/Applications/Docker.app
# }

# Go to web pages?
# /usr/bin/open -a "/Applications/Google Chrome.app" --args 'http://google.com/'
  # This is a command that works. Needs to be boiled down a bit to be effective and versitile.