# short general
alias dc="docker container"
alias di="docker image"
alias dv="docker volume"
alias dn="docker network"
alias db="docker build"
alias dr="docker run"
# ls && ls -a
alias dcll="docker container ls -a"
alias dill="docker image ls -a"
alias dvll="docker volume ls"
alias dnll="docker network ls"
dlsa(){
  clear
  echo '*** Containers ***'
  dcll
  echo '*** Images ***'
  dill
  echo '*** Volumes ***'
  dvll
}
# compose
alias dcu="docker-compose up"
alias dcub="docker-compose up --build"
alias dcud="docker-compose up -d"
alias dcudb="docker-compose up -d --build"
alias dcd="docker-compose down"
dcdk(){ # Nuke different things with -c, -i, -ci, -v, and -a (all)
  dcd
  while [[ $# -gt 0 ]] do
    if [[ $1 == "-c" ]] then
      dck -a
      shift
    elif [[ $1 == "-i" ]] then
      dik -a
      shift
    elif [[ $1 == "-ci" || $1 == "-ic" ]] then
      dck -a
      dik -a
      shift
    elif [[ $1 == "-v" ]] then
      dvk -a
      shift
    elif [[ $1 == "-a" ]] then
      dck -a
      dik -a
      dvk -a
      clear
      shift
    else
      echo 'Inputs not recognized'
      shift
    fi
  done
  dlsa
}
# removal (These are basically just super prune)
dck(){ # Kill all containers
  if [[ $1 == '-a' ]] then
    dc stop $(dc ls -q)
    dc rm $(dc ls -aq)
  else
    while [[ $# -gt 0 ]] do
      dc stop $1
      dc rm $1
      shift
    done
  fi
  dcll
}
dcs(){ # Stop all containers
  if [[ $1 == '-a' ]] then
    dc stop $(dc ls -q)
  else
    while [[ $# -gt 0 ]] do
      dc stop $1
      dc rm $1
      shift
    done
  fi
}
dik(){ # Kill all image
  if [[ $1 == '-a' ]] then
    di rm -f $(di ls -aq)
  else
    while [[ $# -gt 0 ]] do
      di rm $1
      shift
    done
  fi
  dill
}
dvk(){ # Kill all volumes
  if [[ $1 == '-a' ]] then
    dv rm $(dv ls -q)
  else
    while [[ $# -gt 0 ]] do
      dv rm $1
      shift
    done
  fi
  dvll
}
dka(){ # Kill everything
  dck -a
  dik -a
  dvk -a
  clear
  dlsa
}